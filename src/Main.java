import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class Main {
    public static void main(String[] args) {
        System.out.println("Welcome to the Junit tester!");
        System.out.println("----------------------------------------------------------");
        System.out.println("Find the magic number between 1 and 10");

        Result result = JUnitCore.runClasses(JUnit.class);
        for(Failure failure : result.getFailures()) {
            System.out.println("Sorry! Thats not the magic number!");
        }
        if(result.wasSuccessful()) {
            System.out.println("Correct! 10 is the magic number");



        }

        }
    }

