import org.junit.Test;
import static org.junit.Assert.*;

public class JUnit {
    @Test
    public void assertEqual() {
        assertEquals(10, Math.getNumber());
        }

    @Test
    public void assertNotEqual() {
        assertNotEquals(3, Math.getNumber());
    }

    @Test
    public void assertGreaterThan() {
        assertTrue( Math.getNumber() > 0);
    }

    @Test
    public void assertLessThan() {
        assertFalse( Math.getNumber() > 11);
    }

    @Test
    public void assertNullCondition() {
        assertNotNull(Math.getNumber());
    }
    @Test
    public void assertSameCheck() {
        assertNotSame(Math.getNumber(), Math.doSubtraction());
    }

}