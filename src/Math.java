import java.util.InputMismatchException;
import java.util.Scanner;

public class Math {
    public static int getNumber() {
        Scanner input = new Scanner(System.in);
        boolean conInput = true;
        int num = 0;
        do {
            try {
                num = input.nextInt();
                conInput = false;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid input, please enter a number" + "\n");
                input.nextInt();
            }
        } while(conInput);
        return num;
    }
    public static int doSubtraction() {
                int val2 = 1 - 4;
                return val2;
    }
}